#include <Arduino.h>
#include <SPI.h>
#include <LoRa.h>
#include "definitions.h"
#include <Wire.h>
#include <Adafruit_SleepyDog.h>

#include "NMEAParser.h"

NMEAParser nmeaParser;

void ledToggle()
{
  ledState = !ledState;
  digitalWrite(led, ledState);
}

void LoRa_rxMode()
{
  LoRa.disableInvertIQ(); // normal mode
  LoRa.receive();         // set receive mode
}
void onReceive(int packetSize)
{
  String message = "";

  while (LoRa.available())
  {
    message += (char)LoRa.read();
  }
  // ledToggle();

  Serial.println("Gateway Receive: ");
  Serial.println(message);
  lora_RSSI = LoRa.packetRssi();
  lora_SNR = LoRa.packetSnr();
}
void onTxDone()
{
  // Serial.println("TxDone");
  LoRa_rxMode();
}

void setup()
{
  Serial.begin(115200);
  // while (!Serial);
  // setup watchdog
  pinMode(led, OUTPUT);
  digitalWrite(led, LOW);
  int countdownMS = Watchdog.enable(4000);

  LoRa.setPins(csPin, resetPin, irqPin);

  if (!LoRa.begin(lora_frequency))
  {
    Serial.println("LoRa init failed. Check your connections.");
    while (true)
      ; // if failed, do nothing
  }

  Serial.println("LoRa init succeeded.");
  Serial.println();
  Serial.println("LoRa Simple Gateway");
  Serial.println("Only receive messages from nodes");
  Serial.println("Tx: invertIQ enable");
  Serial.println("Rx: invertIQ disable");
  Serial.println();
  // LoRa.setSpreadingFactor(lora_SF); // ranges from 6-12,default 7 see API docs
  // LoRa.setCodingRate4(lora_CR);
  // LoRa.setSignalBandwidth(lora_BW);
  // LoRa.enableCrc();
  // LoRa.setGain(lora_GAIN);
  // LoRa.setTxPower(lora_TX_P, lora_TX_PA);


  LoRa.onReceive(onReceive);
  LoRa.onTxDone(onTxDone);
  LoRa_rxMode();
}
boolean runEvery(unsigned long interval)
{
  static unsigned long previousMillis = 0;
  unsigned long currentMillis = millis();
  if (currentMillis - previousMillis >= interval)
  {
    previousMillis = currentMillis;
    return true;
  }
  return false;
}
void LoRa_txMode()
{
  LoRa.idle();           // set standby mode
  LoRa.enableInvertIQ(); // active invert I and Q signals
}
void LoRa_sendMessage(String message)
{
  Serial.println("sending lora");
  LoRa_txMode();        // set tx mode
  LoRa.beginPacket();   // start packet
  LoRa.print(message);  // add payload
  LoRa.endPacket(true); // finish packet and send it
  LoRa_rxMode();
}
void loop()
{
  Watchdog.reset();
  if (runEvery(5000))
  { // repeat every 5000 millis

    String message = "HeLoRa World! ";
    message += "I'm a Gateway! ";
    message += " RSSI: ";
    message += lora_RSSI;
    message += ", SNR: ";
    message += lora_SNR;
    message += ", SF: ";
    message += lora_SF;
    message += ", BW: ";
    message += lora_BW;
    message += ", CR: ";
    message += lora_CR;
    message += ", GAIN: ";
    message += lora_GAIN;
    message += ", TX_P: ";
    message += lora_TX_P;
    message += ", TX_PA: ";
    message += lora_TX_PA;
    message += ", FREQ: ";
    message += lora_frequency;
    message += "\n" ;
    statusMSG = "$SMSIG,";
    statusMSG += lora_RSSI;
    statusMSG += ",";
    statusMSG += lora_SNR;
    statusMSG += ",";
    statusMSG += lora_BW;
    statusMSG += ",";
    statusMSG += lora_SF;
    statusMSG += ",";
    statusMSG += lora_CR;
    statusMSG += ",";
    statusMSG += lora_GAIN;
    statusMSG += ",";
    statusMSG += lora_TX_P;
    statusMSG += ",";
    statusMSG += lora_TX_PA;
    statusMSG += ",";
    statusMSG += lora_frequency;
    
 String modifiedStatusMSG = nmeaParser.generateAndAppendChecksum(statusMSG);


    LoRa_sendMessage(modifiedStatusMSG);
    Serial.println(modifiedStatusMSG);
    Serial.println(message);
    // Serial.println("Send Message!");
  }
}
