#ifndef NMEA_PARSER_H
#define NMEA_PARSER_H

#include "Arduino.h"

class NMEAParser {
public:
    NMEAParser();
    bool validateSentence(const char* sentence);
    void appendChecksum(char* sentenceWithChecksum, const char* sentence);
    String generateAndAppendChecksum(const String& message);
    
private:
    int getFieldCount(const char* sentence);
    bool verifyChecksum(const char* sentence);
    char generateChecksum(const char* sentence);
};

#endif // NMEA_PARSER_H
