#include "Arduino.h"
#include "NMEAParser.h"
#include <string.h>

NMEAParser::NMEAParser() {}

bool NMEAParser::validateSentence(const char* sentence) {
    // Verify the checksum
    if (!verifyChecksum(sentence)) {
        return false;
    }

    // Additional validation logic if needed
    
    return true;
}

void NMEAParser::appendChecksum(char* sentenceWithChecksum, const char* sentence) {
    // Calculate checksum and append to the original sentence
    char checksum = generateChecksum(sentence);
    sprintf(sentenceWithChecksum, "%s*%02X", sentence, checksum);
}

String NMEAParser::generateAndAppendChecksum(const String& message) {
    // Convert the String message to a character array
    char sentence[100]; // Adjust the array size as needed
    message.toCharArray(sentence, sizeof(sentence));

    // Append checksum to the original sentence using the instance of NMEAParser
    char sentenceWithChecksum[110]; // Adjust the array size as needed
    appendChecksum(sentenceWithChecksum, sentence);

    // Convert the modified sentence to a String and return
    return String(sentenceWithChecksum);
}

int NMEAParser::getFieldCount(const char* sentence) {
    // Count the number of fields in the sentence
    int count = 0;
    char* token = strtok((char*)sentence, ",");
    while (token != NULL) {
        count++;
        token = strtok(NULL, ",");
    }
    return count;
}

bool NMEAParser::verifyChecksum(const char* sentence) {
    // Find the position of the '*' character
    const char* asteriskPos = strchr(sentence, '*');

    // Check if there's an asterisk and at least two characters after it
    if (asteriskPos == NULL || strlen(asteriskPos) < 3) {
        return false;
    }

    // Extract the checksum part from the sentence
    char checksumStr[3];
    strncpy(checksumStr, asteriskPos + 1, 2);
    checksumStr[2] = '\0';

    // Convert the checksum string to an integer
    int expectedChecksum = strtol(checksumStr, NULL, 16);

    // Calculate the checksum for the sentence (excluding the '$' character)
    int calculatedChecksum = 0;
    for (int i = 1; i < asteriskPos - sentence; i++) {
        calculatedChecksum ^= sentence[i];
    }

    return (calculatedChecksum == expectedChecksum);
}

char NMEAParser::generateChecksum(const char* sentence) {
    // Initialize checksum with the first character ('$' character)
    char checksum = sentence[1];

    // XOR each character in the sentence, excluding the '$' character
    for (int i = 2; sentence[i] != '\0' && sentence[i] != '*'; i++) {
        checksum ^= sentence[i];
    }

    return checksum;
}
